
class Person {

    String nombre;
    int edad;

    public Person(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public void compararPersona(Person otra){
        if (this.edad > otra.edad) {
            System.out.println (this.nombre+ " es mayor que "+ otra.nombre);
        } else {
            System.out.println (otra.nombre+ " es mayor que "+ this.nombre);
        }
    }
}


